local wibox = require('wibox')
local awful = require('awful')
local gears = require('gears')

-- local weather_buttons = gears.table.join(
--   awful.button({ }, 1, function(t)
--       awful.spawn.with_shell(". ~/.config/awesome/configuration/util/rofiwifi/rofi-network-manager.sh")
--   end)
-- )

weather = wibox.widget {
    text   = 'No Weather Data!',
    align = 'center',
    valign = 'center',
    buttons = wlan_buttons,
    widget = wibox.widget.textbox,
}

-- For the Weather Checker
awful.widget.watch(
  [[bash -c "curl -Ss 'https://wttr.in?0&T&Q' | cut -c 16- | head -2 | xargs echo"]],
	300,
	function(_, stdout)
    -- If this fails, curl will output that it "could not resolve host", therefore we will search for a unique word such as resolve to identify a curl error.
    if string.match(stdout, "resolve") then
      weather.text = ("No Weather Data!")
    else
      weather.text = (stdout)
    end

		collectgarbage('collect')
	end
)
