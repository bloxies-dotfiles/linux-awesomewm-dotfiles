-- Copyright 2013 mokasin
-- This file is part of the Awesome Pulseaudio Widget (APW).
--
-- APW is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- APW is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with APW. If not, see <http://www.gnu.org/licenses/>.

-- Configuration variables
local width         = 40        -- width in pixels of progressbar
local margin_right  = 0         -- right margin in pixels of progressbar
local margin_left   = 0         -- left margin in pixels of progressbar
local margin_top    = 0         -- top margin in pixels of progressbar
local margin_bottom = 0         -- bottom margin in pixels of progressbar
local step          = 300      -- stepsize for temperature change
local color         = '#698f1e' -- foreground color of progessbar
local color_bg      = '#33450f' -- background color
local color_mute    = '#be2a15' -- foreground color when muted
local color_bg_mute = '#532a15' -- background color when muted
local mixer         = 'pavucontrol' -- mixer command
local show_text     = true     -- show percentages on progressbar
local text_color    = '#fff' -- color of text

-- End of configuration

local awful = require("awful")
local spawn_with_shell = awful.util.spawn_with_shell or awful.spawn.with_shell
local wibox = require("wibox")
local beautiful = require("beautiful")
local redshift = require("modules.redshift")
local math = require("math")
-- default colors overridden by Beautiful theme
color = beautiful.apw_fg_color or color
color_bg = beautiful.apw_bg_color or color_bg
color_mute = beautiful.apw_mute_fg_color or color_mute
color_bg_mute = beautiful.apw_mute_bg_color or color_bg_mute
show_text = beautiful.apw_show_text or show_text
text_color = beautiful.apw_text_colot or text_color

local redshiftBar = wibox.widget.progressbar()

redshiftBar.forced_width = width
redshiftBar.step = step

local redshiftWidget
local redshiftText
if show_text then
  redshiftText = wibox.widget.textbox()
  redshiftText:set_align("center")
  redshiftWidget = wibox.container.margin(wibox.widget {
                                         redshiftBar,
                                         redshiftText,
                                         layout = wibox.layout.stack
                                                    },
                                       margin_right, margin_left,
                                       margin_top, margin_bottom)
else
  redshiftWidget = wibox.container.margin(redshiftBar,
                                       margin_right, margin_left,
                                       margin_top, margin_bottom)
end

function redshiftWidget.setColor(state)
	if state == 0 then
		redshiftBar:set_color(color_off)
		redshiftBar:set_background_color(color_bg_off)
	else
		redshiftBar:set_color(color)
		redshiftBar:set_background_color(color_bg)
	end
end

local function _update()
	redshiftBar:set_value(redshift.temperature / (redshift.max - redshift.min))
	redshiftWidget.setColor(redshift.state)
  if show_text then
    redshiftText:set_markup('<span color="'..text_color..'">'..tostring(redshift.temperature)..'K</span>')
    if redshift.state == 0 then
      redshiftText:set_markup('<span color="'..text_color..'">'.."Off"..'</span>')
    end
  end
end

function redshiftWidget.SetMixer(command)
	mixer = command
end

function redshiftWidget.Up()
	redshift.tempUP(redshiftBar.step)
	_update()
end

function redshiftWidget.Down()
	redshift.tempDOWN(redshiftBar.step)
	_update()
end


function redshiftWidget.Toggle()
  redshift.toggle()
  _update()
end

function redshiftWidget.Update()
  _update()
end

function redshiftWidget.LaunchMixer()
	spawn_with_shell( mixer )
end


-- register mouse button actions
redshiftWidget:buttons(awful.util.table.join(
                      awful.button({ }, 1, redshiftWidget.Toggle),
                      awful.button({ }, 3, redshiftWidget.LaunchMixer),
                      awful.button({ }, 4, redshiftWidget.Up),
                      awful.button({ }, 5, redshiftWidget.Down)
                                         )
)

-- initialize
_update()

-- You could update the widget periodically if you'd like. In case, the temperature is changed from somewhere else.
-- redshifttimer = timer({ timeout = 1 }) -- set update interval in s
-- redshifttimer:connect_signal("timeout", redshiftWidget.Update)
-- redshifttimer:start()

return redshiftWidget
